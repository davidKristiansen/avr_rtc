/*
 *  main.c
 *
 *  DS1302 RTC library for AVR microcontrollers
 *  Copyright (C) 2016  David Kristiansen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#define gotoxy(x,y) printf("\033[%d;%dH", (x), (y))
#define clear_screen() printf("\e[1;1H\e[2J")

#include <avr/io.h>
#define F_CPU 16000000UL
#include <util/delay.h>

#include "lib/rtc/rtc.h"
#include "inc/usart.h"

int main()
{
	USART_init(F_CPU, 9600, 1);
	clear_screen();
	rtc_t *rtc = rtc_init('B', PB0, PB1, PB2);




	rtc_write_second(rtc, 45);
	rtc_write_minute(rtc, 59);
	rtc_write_hour(rtc, 17);

	rtc_write_day(rtc, 24);
	rtc_write_month(rtc, 12);
	rtc_write_year(rtc, 17);

	printf("looping");
	while (1)
	{
		rtc_read_burst(rtc);

		gotoxy(1, 1);
		printf("%02d.%02d 20%02d  ", rtc->day, rtc->month, rtc->year);
		printf("%02d:%02d:%02d", rtc->hour, rtc->minute, rtc->second);
		_delay_ms(1000);
	}
}