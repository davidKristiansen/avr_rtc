/*
 *  usart.c
 *
 *  DS1302 RTC library for AVR microcontrollers
 *  Copyright (C) 2016  David Kristiansen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "usart.h"
#include <avr/io.h>
#include <avr/interrupt.h>

// extern function; declared in main.
//extern void usart_fsm(char);

// File type to stream to stdout
FILE uart_str = FDEV_SETUP_STREAM(USART_TX_SingleByte, NULL, _FDEV_SETUP_RW);

/**
 *
 * @param f_cpu CPU Frequency
 * @param baud_rate UART Baud rate
 * @param use_2x Set to 1 (true) if using 2x
 */
void USART_init(long int f_cpu, uint32_t baud_rate, uint8_t use_2x)
{
	// Calculate the UBRR register value
	uint16_t ubrr_reg = ((f_cpu/(16*baud_rate)-1)*(use_2x ? 2 : 1));

	// If we are using 2X speed; enable it
	if(use_2x)
		UCSR0A |= (1 << U2X0);
	else
		UCSR0A &= ~(1 << U2X0);

	// Enable RX, TX, and interrupt on RX
	UCSR0B |= (1 << RXCIE0) | (1 << RXEN0) | (1 << TXEN0);

	// Asynchronous, No Parity, 1 stop, 8-bit data, Falling XCK edge
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ10) | (1 << UCPOL0);

	// Write to UBRR register
	UBRR0H = (ubrr_reg >> 8);
	UBRR0L = ubrr_reg;

	// Send fprint to stdout
	stdout = &uart_str;

	// Enable interrupts
	sei();

}

/**
 *
 * @param cByte Char to be sent over UART
 * @param stream FILE, this can be <0>. Only set to remove a warning from FDEV_SETUP_STREAM function
 * @return int 0 = no error
 */
int USART_TX_SingleByte(char cByte, FILE *stream)
{
	// If byte is newline; also return pointer '\r'
	if(cByte == '\n')
		USART_TX_SingleByte('\r', 0);

	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = cByte;	// Writing to the UDR transmit buffer causes the byte to be transmitted

	return 0;
}

/**
 *
 */
ISR(USART0_RX_vect)
{
	char cData = UDR0;

	// Send usart character to the state machine function
//	usart_fsm(cData);

}