/*
 *  usart.h
 *
 *  DS1302 RTC library for AVR microcontrollers
 *  Copyright (C) 2016  David Kristiansen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef USART_H
#define USART_H

#include <stdio.h>

int USART_TX_SingleByte(char cByte, FILE *stream);
void USART_init(long int f_cpu, uint32_t baud_rate, uint8_t use_2x);



#endif //USART_H
