/*
 *  rtc.c
 *
 *  DS1302 RTC library for AVR microcontrollers
 *  Copyright (C) 2016  David Kristiansen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "rtc.h"

struct ds1302_t {
	volatile uint8_t *reg_port, *reg_ddr, *reg_pin;
	volatile uint8_t pin_clk, pin_io, pin_ce;
};


/* ==================================================================================================================
          INIT FUNCTION
   ================================================================================================================== */
/*! Initializer for Real time clock Struct
 * Example usage: 'rtc_t *rtc = rtc_init('B', PB0, PB1, PB2);'
 *
 * The rtc object has to be part of the input parameter.
 * I.E rtc_get_year(rtc);
 *
 *
 * @param _port     Name of the PORT ex: 'B'
 * @param _pin_clk  Clock Pin
 * @param _pin_io   IO Pin
 * @param _pin_ce   CE Pin sometimes named rst
 * @return          A Pointer to a rtc_t type
 */
rtc_t *rtc_init(uint8_t _port, uint8_t _pin_clk, uint8_t _pin_io, uint8_t _pin_ce)
{
	// Allocate memory for structs
	rtc_t *__rtc_ptr = malloc(sizeof(rtc_t));
	__rtc_ptr->ds1302 = malloc(sizeof *__rtc_ptr->ds1302);

	// Check for which port we are using
	switch (_port)
	{
		case 'a':
		case 'A':
			__rtc_ptr->ds1302->reg_ddr  = &DDRA;
			__rtc_ptr->ds1302->reg_port = &PORTA;
			__rtc_ptr->ds1302->reg_pin  = &PINA;
			break;
		case 'b':
		case 'B':
			__rtc_ptr->ds1302->reg_ddr  = &DDRB;
			__rtc_ptr->ds1302->reg_port = &PORTB;
			__rtc_ptr->ds1302->reg_pin  = &PINB;
			break;
		case 'c':
		case 'C':
			__rtc_ptr->ds1302->reg_ddr  = &DDRC;
			__rtc_ptr->ds1302->reg_port = &PORTC;
			__rtc_ptr->ds1302->reg_pin  = &PINC;
			break;
		case 'd':
		case 'D':
			__rtc_ptr->ds1302->reg_ddr  = &DDRD;
			__rtc_ptr->ds1302->reg_port = &PORTD;
			__rtc_ptr->ds1302->reg_pin  = &PIND;
			break;
		case 'e':
		case 'E':
			__rtc_ptr->ds1302->reg_ddr  = &DDRE;
			__rtc_ptr->ds1302->reg_port = &PORTE;
			__rtc_ptr->ds1302->reg_pin  = &PINE;
			break;
		case 'f':
		case 'F':
			__rtc_ptr->ds1302->reg_ddr  = &DDRF;
			__rtc_ptr->ds1302->reg_port = &PORTF;
			__rtc_ptr->ds1302->reg_pin  = &PINF;
			break;
		case 'g':
		case 'G':
			__rtc_ptr->ds1302->reg_ddr  = &DDRG;
			__rtc_ptr->ds1302->reg_port = &PORTG;
			__rtc_ptr->ds1302->reg_pin  = &PING;
			break;
		case 'h':
		case 'H':
			__rtc_ptr->ds1302->reg_ddr  = &DDRH;
			__rtc_ptr->ds1302->reg_port = &PORTH;
			__rtc_ptr->ds1302->reg_pin  = &PINH;
			break;
		case 'j':
		case 'J':
			__rtc_ptr->ds1302->reg_ddr  = &DDRJ;
			__rtc_ptr->ds1302->reg_port = &PORTJ;
			__rtc_ptr->ds1302->reg_pin  = &PINJ;
			break;
		case 'k':
		case 'K':
			__rtc_ptr->ds1302->reg_ddr  = &DDRK;
			__rtc_ptr->ds1302->reg_port = &PORTK;
			__rtc_ptr->ds1302->reg_pin  = &PINK;
			break;
		case 'l':
		case 'L':
			__rtc_ptr->ds1302->reg_ddr  = &DDRL;
			__rtc_ptr->ds1302->reg_port = &PORTL;
			__rtc_ptr->ds1302->reg_pin  = &PINL;
			break;
		default:
			__rtc_ptr->ds1302->reg_ddr  = 0;
			__rtc_ptr->ds1302->reg_port = 0;
			__rtc_ptr->ds1302->reg_pin  = 0;
	}

	// Set pins
	__rtc_ptr->ds1302->pin_clk = _pin_clk;
	__rtc_ptr->ds1302->pin_io  = _pin_io;
	__rtc_ptr->ds1302->pin_ce  = _pin_ce;

	// Set all pins as output
	*__rtc_ptr->ds1302->reg_ddr |=
		((1<<__rtc_ptr->ds1302->pin_clk) | (1<<__rtc_ptr->ds1302->pin_io) | (1<<__rtc_ptr->ds1302->pin_ce));

	// Set all pins to zero
	*__rtc_ptr->ds1302->reg_port &=
		~((1<<__rtc_ptr->ds1302->pin_clk) | (1<<__rtc_ptr->ds1302->pin_io) | (1<<__rtc_ptr->ds1302->pin_ce));

	// Clear the write protect bit
	ds1302_write_byte(__rtc_ptr->ds1302, WRITE_PROT_ADDR, 0);

	// Return the rtc pointer
	return __rtc_ptr;
}

/* ==================================================================================================================
          PUBLIC FUNCTIONS
   ================================================================================================================== */

/*! Push the year to the rtc module
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @param c_byte    8-bit data
 */
void rtc_write_year(struct rtc_t *_rtc_ptr, uint8_t _data)
{
	uint8_t __tmp;

	__tmp = __dec2bcd(_data) & (YEAR_MASK);
	ds1302_write_byte(_rtc_ptr->ds1302, YEAR_WRITE_ADDR, __tmp);
}

/*! Push the month to the rtc module
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @param _data     8-bit data
 */
void rtc_write_month(struct rtc_t *_rtc_ptr, uint8_t _data)
{
	uint8_t __tmp;

	__tmp = __dec2bcd(_data) & (MONTH_MASK);
	ds1302_write_byte(_rtc_ptr->ds1302, MONTH_WRITE_ADDR, __tmp);
}

/*! Push the weekday to the rtc module
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @param _data     8-bit data
 */
void rtc_write_weekday(struct rtc_t *_rtc_ptr, uint8_t _data)
{
	uint8_t __tmp;

	__tmp = __dec2bcd(_data) & (WEEKDAY_MASK);
	ds1302_write_byte(_rtc_ptr->ds1302, WEEKDAY_WRITE_ADDR, __tmp);
}

/*! Push the day to the rtc module
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @param _data     8-bit data
 */
void rtc_write_day(struct rtc_t *_rtc_ptr, uint8_t _data)
{
	uint8_t __tmp;

	__tmp = __dec2bcd(_data) & (DAY_MASK);
	ds1302_write_byte(_rtc_ptr->ds1302, DAY_WRITE_ADDR, __tmp);
}

/*! Push the hour to the rtc module
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @param _data     8-bit data
 */
void rtc_write_hour(struct rtc_t *_rtc_ptr, uint8_t _data)
{
	uint8_t __tmp;

	__tmp = (__dec2bcd(_data) & (HOUR_MASK));
	ds1302_write_byte(_rtc_ptr->ds1302, HOUR_WRITE_ADDR, __tmp);
}

/*! Push the minute to the rtc module
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @param _data     8-bit data
 */
void rtc_write_minute(struct rtc_t *_rtc_ptr, uint8_t _data)
{
	uint8_t __tmp;

	__tmp = __dec2bcd(_data) & (MINUTE_MASK);
	ds1302_write_byte(_rtc_ptr->ds1302, MINUTE_WRITE_ADDR, __tmp);
}

/*! Push the second to the rtc module
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @param _data     8-bit data
 */
void rtc_write_second(struct rtc_t *_rtc_ptr, uint8_t _data)
{
	uint8_t __tmp;

	__tmp = __dec2bcd(_data) & (SECOND_MASK);
	ds1302_write_byte(_rtc_ptr->ds1302, SECOND_WRITE_ADDR, __tmp);
}
/*! Pull the year from the rtc module
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @return          Year. already formatted to base 10
 */
uint8_t rtc_read_year(struct rtc_t *_rtc_ptr)
{
	uint8_t __tmp;

	__tmp = ds1302_read_byte(_rtc_ptr->ds1302, YEAR_READ_ADDR);
	_rtc_ptr->year = ((__tmp & YEAR_MASK)>>4)*10 + (__tmp & 0x0F);

	return _rtc_ptr->year;
}

/*! Pull the month from the rtc module
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @return          Month. already formatted to base 10
 */
uint8_t rtc_read_month(struct rtc_t *_rtc_ptr)
{
	uint8_t __tmp;

	__tmp = ds1302_read_byte(_rtc_ptr->ds1302, MONTH_READ_ADDR);
	_rtc_ptr->month = ((__tmp & MONTH_MASK)>>4)*10 + (__tmp & 0x0F);

	return _rtc_ptr->month;
}

/*! Pull the weekday from the rtc module
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @return          Weekday. already formatted to base 10
 */
uint8_t rtc_read_weekday(struct rtc_t *_rtc_ptr)
{
	uint8_t __tmp;

	__tmp = ds1302_read_byte(_rtc_ptr->ds1302, WEEKDAY_READ_ADDR);
	_rtc_ptr->weekday = ((__tmp & WEEKDAY_MASK)>>4)*10 + (__tmp & 0x0F);

	return _rtc_ptr->weekday;
}

/*! Pull the day from the rtc module
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @return          Day. already formatted to base 10
 */
uint8_t rtc_read_day(struct rtc_t *_rtc_ptr)
{
	uint8_t __tmp;

	__tmp = ds1302_read_byte(_rtc_ptr->ds1302, DAY_READ_ADDR);
	_rtc_ptr->day = ((__tmp & DAY_MASK)>>4)*10 + (__tmp & 0x0F);

	return _rtc_ptr->day;
}

/*! Pull the hour from the rtc module
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @return          Hour. already formatted to base 10
 */
uint8_t rtc_read_hour(struct rtc_t *_rtc_ptr)
{
	uint8_t __tmp;

	__tmp = ds1302_read_byte(_rtc_ptr->ds1302, HOUR_READ_ADDR);
	_rtc_ptr->hour = ((__tmp & HOUR_MASK)>>4)*10 + (__tmp & 0x0F);

	return _rtc_ptr->hour;
}

/*! Pull the minute from the rtc module
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @return          Minute. already formatted to base 10
 */
uint8_t rtc_read_minute(struct rtc_t *_rtc_ptr)
{
	uint8_t __tmp;

	__tmp = ds1302_read_byte(_rtc_ptr->ds1302, MINUTE_READ_ADDR);
	_rtc_ptr->minute = ((__tmp & MINUTE_MASK)>>4)*10 + (__tmp & 0x0F);

	return _rtc_ptr->minute;
}

/*! Pull the second from the rtc module
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @return          Second. already formatted to base 10
 */
uint8_t rtc_read_second(struct rtc_t *_rtc_ptr)
{
	uint8_t __tmp;

	__tmp = ds1302_read_byte(_rtc_ptr->ds1302, SECOND_READ_ADDR);
	_rtc_ptr->second = ((__tmp & SECOND_MASK)>>4)*10 + (__tmp & 0x0F);

	return _rtc_ptr->second;
}

/*! Burst read from the rtc module
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 */
void rtc_read_burst(struct rtc_t *_rtc_ptr)
{
	uint8_t __tmp[8];

	ds1302_read_burst(_rtc_ptr->ds1302,
	                  &__tmp[0], &__tmp[1], &__tmp[2], &__tmp[3], &__tmp[4], &__tmp[5], &__tmp[6]);

	_rtc_ptr->second   = ((__tmp[0] & SECOND_MASK)  >>4)*10 + (__tmp[0] & 0x0F);
	_rtc_ptr->minute   = ((__tmp[1] & MINUTE_MASK)  >>4)*10 + (__tmp[1] & 0x0F);
	_rtc_ptr->hour     = ((__tmp[2] & HOUR_MASK)    >>4)*10 + (__tmp[2] & 0x0F);
	_rtc_ptr->day      = ((__tmp[3] & DAY_MASK)     >>4)*10 + (__tmp[3] & 0x0F);
	_rtc_ptr->month    = ((__tmp[4] & MONTH_MASK)   >>4)*10 + (__tmp[4] & 0x0F);
	_rtc_ptr->weekday  = ((__tmp[5] & WEEKDAY_MASK) >>4)*10 + (__tmp[5] & 0x0F);
	_rtc_ptr->year     = ((__tmp[6] & YEAR_MASK)    >>4)*10 + (__tmp[6] & 0x0F);
}

/*! Burst write to the rtc module
 *
 * @param _rtc_ptr Pointer to rtc_t struct
 * @param _second
 * @param _minute
 * @param _hour
 * @param _day
 * @param _month
 * @param _weekday
 * @param _year
 */
void rtc_write_burst(struct rtc_t *_rtc_ptr,
                        uint8_t _second, uint8_t _minute, uint8_t _hour,
                        uint8_t _day, uint8_t _month, uint8_t _weekday, uint8_t _year)
{
	ds1302_write_burst(_rtc_ptr->ds1302, _second, _minute, _hour, _day, _month, _weekday, _year);
}

/*! Write byte to rtc RAM
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @param _register
 * @param _data
 */
void rtc_write_ram(struct rtc_t *_rtc_ptr, uint8_t _register, uint8_t _data)
{
	if(_register < 31)
	{
		ds1302_write_byte(_rtc_ptr->ds1302, RAM_WRITE_BASE_ADDR + _register*2, _data);
	}
}

/*!  Read byte from rtc RAM
 *
 * @param _rtc_ptr   Pointer to rtc_t struct
 * @param _register  Register {0--30}
 * @return           8-bit value
 */
uint8_t rtc_read_ram(struct rtc_t *_rtc_ptr, uint8_t _register)
{
	if(_register < 31)
	{
		return	ds1302_read_byte(_rtc_ptr->ds1302, RAM_READ_BASE_ADDR + _register*2);
	}
}

/*! Burst read from RTC RAM
 *
 * @param _rtc_ptr Pointer to rtc_t struct
 * @param _data    data array pointer should be of size 31 to hold all data!
 */
void rtc_read_ram_burst (struct rtc_t *_rtc_ptr, uint8_t *_data)
{

	ds1302_read_ram_burst(_rtc_ptr->ds1302, _data);
}

/*!
 *
 * @param _rtc_ptr  Pointer to rtc_t struct
 * @param _data
 */
void rtc_write_ram_burst (struct rtc_t *_rtc_ptr, uint8_t _data[31])
{
	ds1302_write_ram_burst(_rtc_ptr->ds1302, _data);
}

/*! Convert from decimal numbers to bianry coded decimal for the ds1302 module
 *
 * @param _data  Data to convert
 * @return       Converted data
 */
uint8_t __dec2bcd(uint8_t _data)
{
	return (_data / 10) << 4 | (_data % 10);
}
/* ==================================================================================================================
          PLATFORM SPECIFIC FUNCTIONS (DS1302)
   ================================================================================================================== */
/*! Helper function to set pins
 *
 * @param _ds1302_ptr  Pointer to ds1302 struct
 * @param pin          Pin. IE PB0, 0, or struct->pin_clk
 * @param _val         Value you want to set {0,1}
 */
void ds1302_set_pin(struct ds1302_t *_ds1302_ptr, uint8_t pin, uint8_t _val)
{
	if(_val)
		*_ds1302_ptr->reg_port |= (1<<pin);
	else
		*_ds1302_ptr->reg_port &= ~(1<<pin);
}

/*! Low level write function
 *
 * @param _ds1302_ptr  Pointer to ds1302 struct
 * @param _data       8-bit data to write
 */
void ds1302_write(struct ds1302_t *_ds1302_ptr, uint8_t _data)
{
	uint8_t __i;

	// Ensure IO pin is set as output
	*_ds1302_ptr->reg_ddr |= (1<<_ds1302_ptr->pin_io);

	// Writing starts at LSB
	for (int __i = 0; __i <= 7; ++__i)
	{
		if(_data & (1<<__i))
			*_ds1302_ptr->reg_port |= (1<<_ds1302_ptr->pin_io);
		else
			*_ds1302_ptr->reg_port &= ~(1<<_ds1302_ptr->pin_io);

		// Toggle clk pin
		ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 0);
		ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 1);
	}
}

/*! Low level read function
 *
 * @param _ds1302_ptr  Pointer to ds1302 struct
 * @return             8-bit data from the ds1302 module
 */
uint8_t ds1302_read(struct ds1302_t *_ds1302_ptr)
{
	uint8_t __i, __data = 0;

	// Ensure IO pin is set as input
	*_ds1302_ptr->reg_ddr &= ~(1 << _ds1302_ptr->pin_io);

	// Reading starts at LSB
	for (int __i = 0; __i <= 7; __i++)
	{
		// Toggle clk pin
		ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 1);
		ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 0);

		if(*_ds1302_ptr->reg_pin & (1<<_ds1302_ptr->pin_io))
			__data |= 1 << __i;
		else
			__data &= ~(1 << __i);
	}
	return __data;
}

/*! High level write function
 *
 * @param _ds1302_ptr  Pointer to ds1302 struct
 * @param _addr        8-bit address to write to
 * @param _data        8-bit data from ds1302 module
 */
void ds1302_write_byte(struct ds1302_t *_ds1302_ptr, uint8_t _addr, uint8_t _data)
{
	// Set clk pin low
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 0);

	// Set rst pin high; ensuring rising edge
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_ce, 0);
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_ce, 1);

	ds1302_write(_ds1302_ptr, _addr);
	ds1302_write(_ds1302_ptr, _data);

	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 0);
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_io, 0);
}

/*! High level read function
 *
 * @param _ds1302_ptr  Pointer to ds1302 struct
 * @param _addr        8-bit address to read from
 * @return             8-bit data from ds1302 module
 */
uint8_t ds1302_read_byte(struct ds1302_t *_ds1302_ptr, uint8_t _addr)
{
	uint8_t __data;

	// Set clk pin low
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 0);

	// Set rst pin high; ensuring rising edge
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_ce, 0);
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_ce, 1);

	ds1302_write(_ds1302_ptr, _addr);
	__data = ds1302_read(_ds1302_ptr);

	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 0);
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_io, 0);

	return __data;
}

/*! Burst read from the ds1302 module
 *
 * @param _ds1302_ptr Pointer to ds1302 struct
 * @param _second
 * @param _minute
 * @param _hour
 * @param _day
 * @param _month
 * @param _weekday
 * @param _year
 */
void ds1302_read_burst(struct ds1302_t *_ds1302_ptr,
                       uint8_t *_second, uint8_t *_minute, uint8_t *_hour,
                       uint8_t *_day, uint8_t *_month, uint8_t *_weekday, uint8_t *_year)
{
	// Set clk pin low
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 0);

	// Set rst pin high; ensuring rising edge
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_ce, 0);
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_ce, 1);

	ds1302_write(_ds1302_ptr, BURST_READ_ADDR);
	*_second  = ds1302_read(_ds1302_ptr);
	*_minute  = ds1302_read(_ds1302_ptr);
	*_hour    = ds1302_read(_ds1302_ptr);
	*_day     = ds1302_read(_ds1302_ptr);
	*_month   = ds1302_read(_ds1302_ptr);
	*_weekday = ds1302_read(_ds1302_ptr);
	*_year    = ds1302_read(_ds1302_ptr);

	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 0);
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_io, 0);
}

/*! Burst write to the ds1302 module
 *
 * @param _ds1302_ptr Pointer to ds1302 struct
 * @param _second
 * @param _minute
 * @param _hour
 * @param _day
 * @param _month
 * @param _weekday
 * @param _year
 */
void ds1302_write_burst(struct ds1302_t *_ds1302_ptr,
                       uint8_t _second, uint8_t _minute, uint8_t _hour,
                       uint8_t _day, uint8_t _month, uint8_t _weekday, uint8_t _year)
{
	// Set clk pin low
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 0);

	// Set rst pin high; ensuring rising edge
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_ce, 0);
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_ce, 1);

	ds1302_write(_ds1302_ptr, BURST_WRITE_ADDR);
	ds1302_write(_ds1302_ptr, _second);
	ds1302_write(_ds1302_ptr, _minute);
	ds1302_write(_ds1302_ptr, _hour);
	ds1302_write(_ds1302_ptr, _day);
	ds1302_write(_ds1302_ptr, _month);
	ds1302_write(_ds1302_ptr, _weekday);
	ds1302_write(_ds1302_ptr, _year);

	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 0);
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_io, 0);
}

/*!
 *
 * @param _ds1302_ptr  Pointer to ds1302 struct
 * @param _data
 */
void ds1302_read_ram_burst(struct ds1302_t *_ds1302_ptr, uint8_t *_data)
{
	uint8_t __i;

	// Set clk pin low
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 0);

	// Set rst pin high; ensuring rising edge
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_ce, 0);
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_ce, 1);

	ds1302_write(_ds1302_ptr, BURST_READ_RAM_ADDR);
	for (int __i = 0; __i <= 30 ; __i++)
	{
		*_data++  = ds1302_read(_ds1302_ptr);
	}


	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 0);
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_io, 0);
}

/*!
 *
 * @param _ds1302_ptr  Pointer to ds1302 struct
 * @param _data
 */
void ds1302_write_ram_burst(struct ds1302_t *_ds1302_ptr, uint8_t _data[31])
{
	uint8_t __i;

	// Set clk pin low
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 0);

	// Set rst pin high; ensuring rising edge
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_ce, 0);
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_ce, 1);

	ds1302_write(_ds1302_ptr, BURST_WRITE_RAM_ADDR);
	for (int __i = 0; __i <= 30 ; __i++)
	{
		ds1302_write(_ds1302_ptr, _data[__i]);
	}


	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_clk, 0);
	ds1302_set_pin(_ds1302_ptr, _ds1302_ptr->pin_io, 0);
}