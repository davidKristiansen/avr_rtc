/*
 *  rtc.h
 *
 *  DS1302 RTC library for AVR microcontrollers
 *  Copyright (C) 2016  David Kristiansen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef RTC_H
#define RTC_H

#define WRITE_PROT_ADDR         0x8E

#define YEAR_READ_ADDR          0x8D
#define MONTH_READ_ADDR         0x89
#define DAY_READ_ADDR           0x87
#define HOUR_READ_ADDR          0x85
#define MINUTE_READ_ADDR        0x83
#define SECOND_READ_ADDR        0x81
#define WEEKDAY_READ_ADDR       0x8B
#define BURST_READ_ADDR         0xBF
#define BURST_READ_RAM_ADDR     0xFF
#define RAM_READ_BASE_ADDR      0xC1

#define YEAR_WRITE_ADDR         0x8C
#define MONTH_WRITE_ADDR        0x88
#define DAY_WRITE_ADDR          0x86
#define HOUR_WRITE_ADDR         0x84
#define MINUTE_WRITE_ADDR       0x82
#define SECOND_WRITE_ADDR       0x80
#define WEEKDAY_WRITE_ADDR      0x8A
#define BURST_WRITE_ADDR        0xBE
#define BURST_WRITE_RAM_ADDR    0xBE
#define RAM_WRITE_BASE_ADDR     0xC0

#define YEAR_MASK               0xFF
#define MONTH_MASK              0x1F
#define DAY_MASK                0x3F
#define HOUR_MASK               0x3F
#define MINUTE_MASK             0x7F
#define SECOND_MASK             0x7F
#define WEEKDAY_MASK            0x07


#include <avr/io.h>
#include <stdlib.h>

struct ds1302_t;

typedef struct rtc_t {
	struct ds1302_t *ds1302;

	uint8_t year, month, weekday, day, hour, minute, second;

} rtc_t;

/* ==================================================================================================================
          PUBLIC FUNCTIONS
   ================================================================================================================== */
rtc_t  *rtc_init                (uint8_t _port, uint8_t _pin_clk, uint8_t _pin_io, uint8_t _pin_ce);

void    rtc_write_year          (struct rtc_t *_rtc_ptr, uint8_t _data);
void    rtc_write_month         (struct rtc_t *_rtc_ptr, uint8_t _data);
void    rtc_write_weekday       (struct rtc_t *_rtc_ptr, uint8_t _data);
void    rtc_write_day           (struct rtc_t *_rtc_ptr, uint8_t _data);
void    rtc_write_hour          (struct rtc_t *_rtc_ptr, uint8_t _data);
void    rtc_write_minute        (struct rtc_t *_rtc_ptr, uint8_t _data);
void    rtc_write_second        (struct rtc_t *_rtc_ptr, uint8_t _data);
void    rtc_write_burst         (struct rtc_t *_rtc_ptr,
                                     uint8_t _second, uint8_t _minute, uint8_t _hour,
                                     uint8_t _day, uint8_t _month, uint8_t _weekday, uint8_t _year);
void    rtc_write_ram_burst     (struct rtc_t *_rtc_ptr, uint8_t _data[31]);
void    rtc_write_ram           (struct rtc_t *_rtc_ptr, uint8_t _register, uint8_t _data);

uint8_t rtc_read_year           (struct rtc_t *_rtc_ptr);
uint8_t rtc_read_month          (struct rtc_t *_rtc_ptr);
uint8_t rtc_read_weekday        (struct rtc_t *_rtc_ptr);
uint8_t rtc_read_day            (struct rtc_t *_rtc_ptr);
uint8_t rtc_read_hour           (struct rtc_t *_rtc_ptr);
uint8_t rtc_read_minute         (struct rtc_t *_rtc_ptr);
uint8_t rtc_read_second         (struct rtc_t *_rtc_ptr);
void    rtc_read_burst          (struct rtc_t *_rtc_ptr);
void    rtc_read_ram_burst      (struct rtc_t *_rtc_ptr, uint8_t *_data);
uint8_t rtc_read_ram            (struct rtc_t *_rtc_ptr, uint8_t _register);


uint8_t __dec2bcd               (uint8_t _data);

/* ==================================================================================================================
          PLATFORM SPECIFIC FUNCTIONS (PRIVATE)
   ================================================================================================================== */
void    ds1302_set_pin          (struct ds1302_t *_ds1302_ptr, uint8_t pin, uint8_t _val);
void    ds1302_write            (struct ds1302_t *_ds1302_ptr, uint8_t _data);
uint8_t ds1302_read             (struct ds1302_t *_ds1302_ptr);
void    ds1302_write_byte       (struct ds1302_t *_ds1302_ptr, uint8_t _addr, uint8_t _data);
uint8_t ds1302_read_byte        (struct ds1302_t *_ds1302_ptr, uint8_t _addr);
void    ds1302_read_burst       (struct ds1302_t *_ds1302_ptr,
                                     uint8_t *_second, uint8_t *_minute, uint8_t *_hour,
                                     uint8_t *_day, uint8_t *_month, uint8_t *_weekday, uint8_t *_year);
void    ds1302_write_burst      (struct ds1302_t *_ds1302_ptr,
                                     uint8_t _second, uint8_t _minute, uint8_t _hour,
                                     uint8_t _day, uint8_t _month, uint8_t _weekday, uint8_t _year);
void    ds1302_read_ram_burst   (struct ds1302_t *_ds1302_ptr, uint8_t *_data);
void    ds1302_write_ram_burst  (struct ds1302_t *_ds1302_ptr, uint8_t _data[31]);


#endif //RTC_H
